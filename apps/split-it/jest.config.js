module.exports = {
  name: 'split-it',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/split-it',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
