import { Component } from '@angular/core';
import { Message } from '@split-it/api-interfaces';

@Component({
  selector: 'split-it-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
