import { GoBackService } from './../../services/go-back.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'split-it-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public activeLink: string = 'balance';
  constructor(
    private activeRoute: ActivatedRoute,
    private goBackService: GoBackService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.goBackService.setGoBackRoute('');
    this.activeLink = this.setActiveLink(this.router.url);
    // this.activeRoute.params
    // .pipe(
    //   switchMap(x => this.placeService.get(x.name)))
    // .subscribe(x => {
    //   this.place = x;
    //   this.isLoading = false;
    //   this.activePlace = {
    //     targetPlaceId: this.place.id,
    //     value: this.place.isActive
    //   };
    // });
  }

  private setActiveLink(url: string): string {
    return url.substring(url.lastIndexOf('/') + 1).toLocaleLowerCase();
  }
}
