import { CreateExpenseComponent } from './groups/components/create-expense/create-expense.component';
import { HistoryContainerComponent } from './groups/components/history-container/history-container.component';
import { BalanceContainerComponent } from './groups/components/balance-container/balance-container.component';
import { GroupPageComponent } from './groups/pages/group-page/group-page.component';
import { HeaderComponent } from './core/components/header/header.component';
import { Routes } from "@angular/router";
import { HomePageComponent } from './home/home-page/home-page.component';
import { GroupListContainerComponent } from './groups/components/group-list-container/group-list-container.component';
import { CreateGroupPageComponent } from './groups/pages/create-group-page/create-group-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

export enum homeRoutes {
  Groups = 'groups',
  Balance = 'balance',
}

export const appRoutes: Routes = [
  {
    path: 'home',
    component: HomePageComponent,
    children: [
      {
        path: homeRoutes.Groups,
        component: GroupListContainerComponent
      },
      {
        path: homeRoutes.Balance,
        component: HeaderComponent
      },
      { path: '', redirectTo: homeRoutes.Groups, pathMatch: 'full' },
    ]
  },
  {
    path: 'create-group/:type',
    component: CreateGroupPageComponent
  },
  {
    path: 'finance-expense/:id',
    component: CreateExpenseComponent
  },
  {
    path: 'group/:id',
    component: GroupPageComponent,
    children:[
      {
        path: 'balance',
        component: BalanceContainerComponent
      },
      {
        path: 'history',
        component: HistoryContainerComponent
      },
      { path: '', redirectTo: 'balance', pathMatch: 'full'}
    ]
  },
  {
    path: 'not-found',
    component: NotFoundPageComponent
  },
  { path: '', redirectTo: `/home/${homeRoutes.Groups}`, pathMatch: 'full' },
  { path: '**', redirectTo: '/not-found' }
];

