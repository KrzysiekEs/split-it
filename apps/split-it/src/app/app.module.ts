import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routing';
import { HomeModule } from './home/home.module';
import { GroupsModule } from './groups/groups.module';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

import { ApiService } from './services/api.service';
import { GoBackService } from './services/go-back.service';
import { NotificationService } from './services/notification.service';


@NgModule({
  declarations: [AppComponent, NotFoundPageComponent],
  imports: [

    BrowserModule,
    CoreModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes,
      { enableTracing: true }),
    HomeModule,
    GroupsModule
  ],
  providers: [
    NotificationService,
    ApiService,
    GoBackService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
