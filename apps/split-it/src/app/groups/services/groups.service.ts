import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { GroupDTO, AddGroupDTO } from '@split-it/api-interfaces';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  private readonly activeGroup = new BehaviorSubject<GroupDTO>(undefined);
  private activeGroup$ = this.activeGroup.asObservable();

  constructor(private apiService: ApiService) {}

  sendActiveGroup(activeGroup: GroupDTO) {
    this.activeGroup.next(activeGroup);
  }

  getActiveGroup(): Observable<GroupDTO> {
    return this.activeGroup$;
  }

  getAll(): Observable<GroupDTO[]> {
    return this.apiService
      .get(`groups`)
      .pipe(map(data => data));
  }

  getById(id): Observable<GroupDTO> {
    return this.apiService
      .get(`groups/${id}`)
      .pipe(map(data => data));
  }

  add(addGroupDto: AddGroupDTO): Observable<any>  {
    return this.apiService
      .post('groups', addGroupDto)
  }

  update(group: GroupDTO): Observable<any> {
    return this.apiService
      .put('groups', group);
  }
}
