import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetExpenseDto } from '@split-it/api-interfaces';

@Injectable()
export class ExpensesService {

  constructor(private apiService: ApiService) {}

  getAll(): Observable<GetExpenseDto[]> {
    return this.apiService
      .get(`expenses`)
      .pipe(map(data => data));
  }

  getAllByGroup(id): Observable<GetExpenseDto[]> {
    return this.apiService
      .get(`expenses/group/${id}`)
      .pipe(map(data => data));
  }

  getById(id): Observable<GetExpenseDto> {
    return this.apiService
      .get(`expenses/${id}`)
      .pipe(map(data => data));
  }

  create(body): Observable<any> {
    return this.apiService
      .post(`expenses`, body)
      .pipe(map(data => data));
  }
}
