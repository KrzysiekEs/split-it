import { Component, Input } from '@angular/core';

@Component({
    selector: 'members-list',
    styleUrls: [
        'members-list.component.scss'
    ],
    template: `
        <div class="block" *ngIf="members?.length > 0">
            <header class="item-title">{{ strings?.members }}</header>
            <mat-icon class="right" (click)="deleteSelected(selected.selectedOptions.selected)">delete</mat-icon>
        </div>

        <mat-selection-list #selected>
            <mat-list-option *ngFor="let member of members">
            {{ member }}
            </mat-list-option>
        </mat-selection-list>
    `
})
export class MembersListComponent {
    @Input() members: string[];

    public strings = {
        "members": "Członkowie"
    }

    public deleteSelected(elements: Array<any>) {
        elements.forEach(e => {
          this.members = 
            this.members.filter(m => m !== e._element.nativeElement.innerText);
        })
      }

}