import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExpensesService } from '../../services/expenses.service';
import { GetExpenseDto } from '@split-it/api-interfaces';
import { tap, switchMap, filter, takeUntil } from 'rxjs/operators';
import { GroupsService } from '../../services/groups.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'split-it-history-container',
  templateUrl: './history-container.component.html',
  styleUrls: ['./history-container.component.scss']
})
export class HistoryContainerComponent implements OnInit, OnDestroy {

  public expenses: GetExpenseDto[];
  public isLoading: boolean;
  destroySubject$: Subject<void> = new Subject();
  public group$;

  constructor(
    private expensesService: ExpensesService,
    private groupsService: GroupsService
  ) { }

  ngOnInit(): void {
    this.group$ = this.groupsService
      .getActiveGroup()
      .pipe(
        takeUntil(this.destroySubject$),
        tap(() => this.isLoading = true),
        filter(x => x !== undefined),
        switchMap(x => this.expensesService.getAllByGroup(x._id))
      ).subscribe(expenses => {
        this.expenses = expenses;
        this.isLoading = false
      });


  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
  }

}
