import { GroupsService } from './../../services/groups.service';
import { NotificationService, NotificationType, NotificationMessage } from './../../../services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ExpensesService } from '../../services/expenses.service';
import { GoBackService } from '../../../services/go-back.service';
import { tap, switchMap } from 'rxjs/operators';
import { GroupDTO } from '@split-it/api-interfaces';

@Component({
  selector: 'split-it-create-expense',
  templateUrl: './create-expense.component.html',
  styleUrls: ['./create-expense.component.scss']
})
export class CreateExpenseComponent implements OnInit {

  public currencies = ['PLN', 'USD', 'EUR'];
  public members: string[] = [];
  public selectedMembers = [];
  public expenseForm;
  public groupId: string;
  public expenseType: string = 'equal';
  // public expenseType: string = 'differential';

  public membersSelectionType: 'all' | 'single' = 'all';

  public differentialValues = [];
  public differentialPrice = 0;

  constructor(
    private fb: FormBuilder,
    private expensesService: ExpensesService,
    private groupsService: GroupsService,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private goBackService: GoBackService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        tap(x => this.goBackService.setGoBackRoute(`./group/${x.id}`)),
        tap(params => this.groupId = params.id),
        switchMap(params => this.groupsService.getById(params.id))
      )
      .subscribe((group: GroupDTO) => {
      this.members = group.members;
      this.members.forEach(() => this.differentialValues.push(0));
      this.expenseForm = this.fb.group({
        description: [''],
        value: [1],
        date: [ new Date()],
        buyerIndex: [],
        type: ['PLN'],
        groupId: [this.groupId]
      });
    })
  }

  saveExpense() {
    const formData = this.expenseForm.value;
    if(this.expenseType === 'equal') {
      formData.membersPayoff = this.calculateEqualExpenses(this.membersSelectionType === 'all' ? -1 : this.selectedMembers, formData.value);
    }

    if(this.expenseType === 'differential'){
      formData.membersPayoff = this.differentialValues;
    }
    this.expensesService.create(formData).subscribe(
      () => this.router.navigateByUrl(`group/${this.groupId}/history`),
      (error) => this.notificationService.sendNotification({
          type: NotificationType.Error,
          text: NotificationMessage.ServerError
        })
    );
  }

  onDifferentialChange() {
    this.differentialPrice = 0;
    this.differentialValues.forEach(x => this.differentialPrice += x);
  }

  calculateEqualExpenses(members, value): string[]{
    const membersExpenses = [];
    const calculation = members !== -1 ? value / members.length : value / this.members.length;
    if(members !== -1) {
      this.members.forEach(x => membersExpenses.push(0));
      members.forEach(x => {
        membersExpenses[x] = calculation;
      });
    } else {
      this.members.forEach(x => membersExpenses.push(calculation));
    }
    return membersExpenses;
  }

}
