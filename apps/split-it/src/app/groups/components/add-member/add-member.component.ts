import { Component, Input, Output, EventEmitter } from "@angular/core";
import { AddMemberModal } from '../../modals/add-member/add-member.modal';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'add-member',
    template: `
        <button (click)="onAddNewMember()"
            mat-stroked-button>{{ strings?.addMemberBtnLabel }}
        </button>
    `
})
export class AddMemberComponent {
    @Input() members: string[];
    @Output() added: EventEmitter<boolean> = new EventEmitter();

    public strings = {
        "addMemberBtnLabel": "Dodaj Członka"
      }

    constructor(
        public dialog: MatDialog,
    ) { }

    public onAddNewMember() {
        const dialogRef = this.dialog.open(AddMemberModal, {
          width: '250px',
          data: ""
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if (result && !this.members.some(m => m === result)) {
            this.members.push(result);
            this.added.emit(true);
          }
        });
      }
}
