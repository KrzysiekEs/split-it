import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: 'balance-view',
    templateUrl: './balance-view.component.html',
    styleUrls: [
        './balance-view.component.scss'
    ]
})
export class BalanceViewComponent implements OnInit {
    @Input() members: string[];
    @Input() payoffs: number[][];

    public strings = {
        "balance": "Bilans"
    }

    public payoffSums: number[];

    ngOnInit(): void {
        this.payoffSums = this.payoffs.map(p => 
            p.reduce((a, b) => a + b));
    }
}
