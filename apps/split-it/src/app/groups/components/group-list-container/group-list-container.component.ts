import { Component, OnInit } from '@angular/core';
import {
  NotificationService,
  NotificationType,
  NotificationMessage } from './../../../services/notification.service';
import { GroupsService } from '../../services/groups.service';
import { tap } from 'rxjs/operators';
import { GroupDTO, GroupType } from '@split-it/api-interfaces';

@Component({
  selector: 'split-it-group-list-container',
  templateUrl: './group-list-container.component.html',
  styleUrls: ['./group-list-container.component.scss']
})
export class GroupListContainerComponent implements OnInit {

  public isLoading: boolean;
  public groups: GroupDTO[];
  public GroupType = GroupType;

  constructor(
    private groupsService: GroupsService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {

    this.groupsService.getAll()
    .pipe(
      tap(() => this.isLoading = true)
    )
    .subscribe(allGroups => {
      this.groups = allGroups
        .map(g => {
          return {
            ...g,
            type: parseInt(g.type as any, 10)
          }
        });
      this.isLoading = false;
    },
    error => this.notificationService.sendNotification({
      type: NotificationType.Error,
      text: NotificationMessage.ServerError
    }))


  }

}
