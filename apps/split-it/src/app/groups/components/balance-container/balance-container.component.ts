import { Component, OnInit } from '@angular/core';
import { GroupsService } from '../../services/groups.service';
import { GroupDTO } from '@split-it/api-interfaces';

@Component({
  selector: 'split-it-balance-container',
  templateUrl: './balance-container.component.html',
  styleUrls: ['./balance-container.component.scss']
})
export class BalanceContainerComponent implements OnInit {

  public group: GroupDTO;

  constructor(private groupsService: GroupsService) { }

  ngOnInit(): void {
    this.groupsService.getActiveGroup().subscribe(x => this.group = x);
  }

  public onMemberAdded() {
    this.groupsService.update(this.group)
      .subscribe();
  }
}
