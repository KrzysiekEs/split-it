import { Component, OnInit } from '@angular/core';
import { AddGroupDTO, GroupType } from '@split-it/api-interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupsService } from '../../services/groups.service';
import { GoBackService } from '../../../services/go-back.service';

@Component({
  selector: 'split-it-create-group-page',
  templateUrl: './create-group-page.component.html',
  styleUrls: ['./create-group-page.component.scss']
})
export class CreateGroupPageComponent implements OnInit {
  public strings = {
    "headerStart": "Nowa grupa",
    "headerFood": "jedzeniowa",
    "headerFinance": "finansowa",
    "header": "",
    "groupNameLabel": "Nazwa",
    "addMemberBtnLabel": "Dodaj Członka",
    "shareLinkLabel": "Udostępnij link",
    "addGroupBtnLabel": "Stwórz grupę"
  }

  public model: AddGroupDTO = {
    name: '',
    members: [],
    owner: '',
    type: null
  }

  constructor(
    private route: ActivatedRoute,
    private httpService: GroupsService,
    private router: Router,
    private goBackService: GoBackService
  ) { }

  ngOnInit(): void {

    this.goBackService.setGoBackRoute('./home');

    let headerSuf: string;
    if (this.route.snapshot.paramMap.get('type') == "finance") {
      this.model.type = GroupType.Finance;
      headerSuf = this.strings.headerFinance;
    } else {
      this.model.type = GroupType.Food;
      headerSuf = this.strings.headerFood;
    }

    this.strings.header = `${this.strings.headerStart} ${headerSuf}`;
  }

  public addGroup() {
    this.httpService.add(this.model)
      .subscribe(x  => {
        this.router.navigate(['']);
      });
  }
}
