import { GoBackService } from './../../../services/go-back.service';
import { GroupsService } from './../../services/groups.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { pipe } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'split-it-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss']
})
export class GroupPageComponent implements OnInit {
  public activeLink: string = 'balance';
  public group;

  constructor(
    private groupsService: GroupsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private goBackService: GoBackService
     )
  {
    this.activeLink = this.router.url.split('\\').pop().split('/').pop();
  }

  ngOnInit(): void {
    this.goBackService.setGoBackRoute('./home');

    this.activatedRoute.params
    .pipe(
      switchMap(x => this.groupsService.getById(x.id))
    ).subscribe(x =>  {
      this.group = x;
      this.groupsService.sendActiveGroup(this.group);

    });
  }

}
