import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GroupListContainerComponent } from './components/group-list-container/group-list-container.component';
import { CreateGroupPageComponent } from './pages/create-group-page/create-group-page.component';
import { GroupsService } from './services/groups.service';
import { CoreModule } from '../core/core.module';
import { GroupPageComponent } from './pages/group-page/group-page.component';
import { BalanceContainerComponent } from './components/balance-container/balance-container.component';
import { HistoryContainerComponent } from './components/history-container/history-container.component';
import { CreateExpenseComponent } from './components/create-expense/create-expense.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExpensesService } from './services/expenses.service';
import { CustomAngularMaterialModule } from '../core/custom-angular-material/custom-angular-material.module';
import { AddMemberModal } from './modals/add-member/add-member.modal';
import { MembersListComponent } from './components/members-list/members-list.component';
import { AddMemberComponent } from './components/add-member/add-member.component';
import { BalanceViewComponent } from './components/balance-view/balance-view.component';

@NgModule({
  declarations: [
    GroupListContainerComponent,
    CreateGroupPageComponent,
    GroupPageComponent,
    BalanceContainerComponent,
    HistoryContainerComponent,
    CreateExpenseComponent,
    AddMemberModal,
    MembersListComponent,
    AddMemberComponent,
    BalanceViewComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
    // CustomAngularMaterialModule
  ],
  providers: [
    GroupsService,
    ExpensesService
  ],
  exports: [
    GroupListContainerComponent,
    CreateGroupPageComponent,
    GroupPageComponent,
    BalanceContainerComponent,
    HistoryContainerComponent,
    CreateExpenseComponent
  ]
})
export class GroupsModule { }
