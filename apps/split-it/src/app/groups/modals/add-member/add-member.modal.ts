import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'modal-add-member',
    template: `
        <form>
            <mat-form-field>
                <mat-label>{{ strings?.labelName }}</mat-label>
                <input matInput
                    name="name"s
                    [(ngModel)]="data">
            </mat-form-field>
        </form>
        <button mat-button (click)="close()" color="warn">{{ strings?.cancel }}</button>
        <button mat-button [mat-dialog-close]="data" color="primary">{{ strings?.accept }}</button>
    `
})
export class AddMemberModal {
    public strings = {
        labelName: "Nazwa nowego członka",
        accept: "Dodaj",
        cancel: "Powrót"
    }

    constructor(
        public dialogRef: MatDialogRef<AddMemberModal>,
        @Inject(MAT_DIALOG_DATA) public data: string
    ) { }
    
    close(): void {
        this.dialogRef.close();
    }
}