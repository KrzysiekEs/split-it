
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';


export const NotificationMessage = {
  ServerError: 'Nie udało sie pobrac danych z serwera'
}

export enum NotificationType {
  Error = 'error',
  Info = 'info'
}

export interface INotification {
  type: NotificationType,
  text: string
}


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notification = new Subject<INotification>();

  sendNotification(notification: INotification) {
      this.notification.next(notification);
  }

  clearNotification() {
      this.notification.next();
  }

  getNotification(): Observable<INotification> {
      return this.notification.asObservable();
  }
}
