import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoBackService {

  private readonly goBackRoute = new BehaviorSubject<string>(undefined);
  private goBAckRoute$ = this.goBackRoute.asObservable();

  constructor() { }

  setGoBackRoute(route) {
    this.goBackRoute.next(route);
  }

  getGoBackRoute() {
    return this.goBAckRoute$;
  }

}
