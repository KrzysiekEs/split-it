import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  private _url: string =  environment.api_url;

  private formatErrors(error: any) {
    return throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http
      .get(`${this._url}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http
      .put(`${this._url}${path}`, body)
      .pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http
      .post(`${this._url}${path}`, body)
      .pipe(catchError(this.formatErrors));
  }

  delete(path, body: Object = {}): Observable<any> {
    body['headers'] = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .delete(`${this._url}${path}`, body)
      .pipe(catchError(this.formatErrors));
  }
}
