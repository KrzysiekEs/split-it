import { GoBackService } from './../../../services/go-back.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'split-it-go-back',
  templateUrl: './go-back.component.html',
  styleUrls: ['./go-back.component.scss']
})
export class GoBackComponent implements OnInit {

  public goBackRoute: string;

  constructor(
    private goBackService: GoBackService
    ){}

    ngOnInit() {
      this.goBackService.getGoBackRoute().subscribe(x => this.goBackRoute = x);
    }

}
