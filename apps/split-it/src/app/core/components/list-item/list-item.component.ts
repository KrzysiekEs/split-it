import { Component, OnInit, Input, TemplateRef } from '@angular/core';


@Component({
  selector: 'split-it-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() left: TemplateRef<any>;
  @Input() main: TemplateRef<any>;
  @Input() right: TemplateRef<any>;
  @Input() options: any ={
    name: 'sss'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
