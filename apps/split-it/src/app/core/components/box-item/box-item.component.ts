import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'split-it-box-item',
  templateUrl: './box-item.component.html',
  styleUrls: ['./box-item.component.scss']
})
export class BoxItemComponent implements OnInit {

  @Input() icon: string;
  constructor() { }

  ngOnInit(): void {
  }

}
