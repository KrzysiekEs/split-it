import { Component, OnInit } from '@angular/core';
import { NotificationService, INotification } from '../../../services/notification.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'split-it-information-bar',
  templateUrl: './information-bar.component.html',
  styleUrls: ['./information-bar.component.scss']
})
export class InformationBarComponent implements OnInit {

  public notification: INotification;

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {

    this.notificationService.getNotification()
                            .pipe(debounceTime(.3))
                            .subscribe(n => this.notification = n);
  }

  public closeNotification(){
    this.notificationService.clearNotification();
  }
}
