import { Component, OnInit, Input } from '@angular/core';
import { IconType } from './icon-types.enum';

@Component({
  selector: 'split-it-icon',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss']
})
export class IconsComponent implements OnInit {

  @Input() size: 'xs' | 'sm' | 'md' | 'lg' | 'xl' = 'md';
  @Input() type: IconType;
  @Input() originalColor: boolean;

  constructor() { }

  ngOnInit() {
  }

}
