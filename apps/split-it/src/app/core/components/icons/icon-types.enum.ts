export enum IconType {
  SportClub = 'sport-club',
  Restaurant = 'restaurant',
  Pub = 'pub',
  Close = 'close',
  Edit = 'edit',
  ChevronLeft = 'chevron-left',
  ChevronUp = 'chevron-up',
  ChevronRight = 'chevron-right',
  ChevronDown = 'chevron-down',
  Hamburger = 'hamburger',
  Finance = 'finance',
}
