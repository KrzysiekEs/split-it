import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { CustomAngularMaterialModule } from './custom-angular-material/custom-angular-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogoComponent } from './components/logo/logo.component';
import { FooterComponent } from './components/footer/footer.component';
import { IconsComponent } from './components/icons/icons.component';
import { BoxItemComponent } from './components/box-item/box-item.component';
import { InformationBarComponent } from './components/information-bar/information-bar.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { GoBackComponent } from './components/go-back/go-back.component';

@NgModule({
  declarations: [
    HeaderComponent,
    LogoComponent,
    FooterComponent,
    IconsComponent,
    BoxItemComponent,
    InformationBarComponent,
    ListItemComponent,
    GoBackComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    CustomAngularMaterialModule
  ],
  exports: [
    CustomAngularMaterialModule,
    HeaderComponent,
    FooterComponent,
    IconsComponent,
    BoxItemComponent,
    InformationBarComponent,
    ListItemComponent,
    GoBackComponent
  ]
})
export class CoreModule { }
