import { Injectable, BadRequestException } from '@nestjs/common';
import { UsersService } from '../users/application/users.service';
import { JwtService } from '@nestjs/jwt';
import { CommandBus } from '@miniroman/cqrs';
import { CreateUserCommand } from '../users/application/createUser/create-user.command';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private bus: CommandBus
  ) {}

  async signup(username: string, password: string) {
    const result = await this.bus.execute(new CreateUserCommand(username, password));
    if(result.isFailure())
      throw new BadRequestException(result.value.reason);

    return this.login({username: result.value.name, userId: result.value.id});
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
