import { Module } from '@nestjs/common';
import { CqrsModule } from '@miniroman/cqrs';
import { UsersService } from './application/users.service';
import { DatabaseModule } from '../database/database.module';
import { usersProviders } from './persistance/providers/users.providers';
import handlers from './application/users.providers';

@Module({
  imports: [DatabaseModule, CqrsModule],
  providers: [
    UsersService,
    ...usersProviders,
    ...handlers
  ],
  exports: [UsersService],
})
export class UsersModule {}
