import { Either, Result } from '@split-it/result-type';
import { ReturningCommand } from "@miniroman/cqrs";
import { User } from '../../domain/user.model';

interface DomainError {
  message: string;
  error?: any;
}

export namespace Error {
  export class ValidationError extends Result<DomainError> {
    public constructor (public reason: string) {
      super(false, {
        message: `Invalid model`
      })
    }
  }
}

export class CreateUserCommand extends ReturningCommand<Either<Error.ValidationError, User>> {
  constructor(public name: string, public password: string) {
    super();
  }
}
