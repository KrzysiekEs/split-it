import { CreateUserCommand, Error } from './create-user.command';
import { CommandHandler, ICommandHandler } from '@miniroman/cqrs';
import { Either, success, failure } from '@split-it/result-type';
import { Inject } from '@nestjs/common';
import { User } from '../../domain/user.model';
import { Model } from 'mongoose';

@CommandHandler(CreateUserCommand)
export class CreateUserCommandHandler implements ICommandHandler<CreateUserCommand> {

  constructor(
    @Inject('USER_MODEL')
    private readonly memberModel: Model<User>,
  ) {}

  async execute(command: CreateUserCommand): Promise<Either<Error.ValidationError, User>> {
    if(!command.name || !command.password)
      return failure(new Error.ValidationError('Name and password can not be empty'));
    const newEntity = await this.memberModel.create({
      name: command.name,
      password: command.password
    });
    return success(newEntity);
  }
}
