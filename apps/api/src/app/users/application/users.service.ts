import { Injectable, Inject } from '@nestjs/common';
import { User } from '../domain/user.model';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USER_MODEL')
    private readonly userModel: Model<User>,) { }

  create(user: { username: string; password: string; }) {
    return this.userModel.create(user);
  }

  async findOne(username: string): Promise<User | undefined> {
    return this.userModel.findOne(user => user.username === username);
  }
}
