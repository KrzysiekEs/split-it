import * as mongoose from 'mongoose';

export const ChangelogSchema = new mongoose.Schema({
  change: Array<Number>(),
  type: String,
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  expenseId: { type: mongoose.Schema.Types.ObjectId, ref: 'Expense'},
  groupId: { type: mongoose.Schema.Types.ObjectId, ref: 'Group'}
})
