import * as mongoose from 'mongoose';

export const StateSchema = {
    payoffs: Array<Array<number>>(),
    unit: String
}
