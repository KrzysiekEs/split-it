import * as mongoose from 'mongoose';
import { StateSchema } from './state.schema';

export const GroupSchema = new mongoose.Schema({
  name: String,
  type: String,
  members: Array<String>(),
  owner: String,
  state: StateSchema
})
