import * as mongoose from 'mongoose';

export const ExpenseSchema = new mongoose.Schema({
  description: String,
  value:  Number,
  type: String,
  date: Date,
  buyerIndex: Number,
  membersPayoff: Array<Number>(),
  groupId: { type: mongoose.Schema.Types.ObjectId, ref: 'Group'}
})
