
import { Connection } from 'mongoose';
import { GroupSchema } from '../../database/schemas/group.schema';

export const groupsProviders = [
  {
    provide: 'GROUP_MODEL',
    useFactory: (connection: Connection) => connection.model('Group', GroupSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
