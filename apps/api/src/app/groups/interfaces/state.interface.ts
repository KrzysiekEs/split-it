/* DOCUMENTATION
    payoffs = 
        [
            [0, -3],
            [3,  0]
        ]
    It means:
        - member with index 0 owns member with index 1 (3 units)
*/

export interface State {
    payoffs: number[][],
    unit: string
}
