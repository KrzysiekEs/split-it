import { Document } from 'mongoose';
import { GroupType } from '@split-it/api-interfaces';
import { State } from './state.interface';

export interface Group extends Document {
  readonly name: string;
  readonly type: GroupType
  readonly members: string[];
  owner: string;
  state: State;
}
