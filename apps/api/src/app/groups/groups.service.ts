import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Group } from './interfaces/group.interface';
import { AddGroupDTO, GroupDTO } from '@split-it/api-interfaces';

@Injectable()
export class GroupsService {
  constructor(
    @Inject('GROUP_MODEL')
    private readonly groupModel: Model<Group>,
  ) {}

  async create(createGroupDto: AddGroupDTO): Promise<Group> {
    const createdGroup = new this.groupModel({
      ...createGroupDto,
      state: {
        payoffs: createGroupDto.members.map(m => createGroupDto.members.map(m => 0)),
        unit: ""
      }
    });
    return createdGroup.save();
  }

  async update(group: GroupDTO): Promise<Group> {
    return await this.groupModel.findByIdAndUpdate({_id: group._id}, {...group});
  }

  async findAll(): Promise<Group[]> {
    return await this.groupModel.find();
  }

  async find(id): Promise<Group> {
    return await this.groupModel.findById({_id: id }).exec();
  }

  async delete(id: string): Promise<Group>{
    return this.groupModel.findOneAndRemove({_id: id}).exec();
  }

  async updateState(id: string, buyerIndex: number, payoffs: number[]) {
    const group = await this.find(id);
    
    payoffs.forEach((value, index) => {
      if (index !== buyerIndex) {
        group.state.payoffs[buyerIndex][index] += value;
        group.state.payoffs[index][buyerIndex] -= value;
      }
    });
    group.state = JSON.parse(JSON.stringify(group.state));
    group.save();
  }
}
