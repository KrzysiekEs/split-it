import { Controller, Get, Post, Body, Delete, Param, Put } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { Group } from './interfaces/group.interface';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { AddGroupDTO, GroupDTO } from '@split-it/api-interfaces';

@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Post()
  @ApiResponse({ status: 201, description: 'The group has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(@Body() addGroupDto: AddGroupDTO) {
    return await this.groupsService.create(addGroupDto);
  }

  @Put()
  async update(@Body() groupDTO: GroupDTO): Promise<Group> {
    return await this.groupsService.update(groupDTO);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Group> {
    return this.groupsService.find(id);
  }

  @Get()
  async findAll(): Promise<Group[]> {
    return this.groupsService.findAll();
  }

  @Delete()
  async delete(@Body() id: string): Promise<Group> {
    return this.groupsService.delete(id);
  }
}
