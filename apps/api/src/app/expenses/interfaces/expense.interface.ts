import { Document } from 'mongoose';

export interface Expense extends Document {
  readonly description: string;
  readonly type: string;
  readonly date: Date;
  readonly buyerIndex: number;
  readonly membersPayoff: number[];
  readonly groupId: string;
}
