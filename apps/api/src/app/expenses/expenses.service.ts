
import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { ApiResponse } from '@nestjs/swagger';
import { CreateExpenseDto, ExpenseDto } from '@split-it/api-interfaces';
import { Expense } from './interfaces/expense.interface';
import { GroupsService } from '../groups/groups.service';

@Injectable()
export class ExpensesService {
  constructor(
    @Inject('EXPENSE_MODEL')
    private readonly expenseModel: Model<Expense>,
    private readonly groupsService: GroupsService
  ) {}

  async create(dto: CreateExpenseDto): Promise<ExpenseDto> {
    const createdExpense = new this.expenseModel(dto);
    this.groupsService.updateState(dto.groupId, dto.buyerIndex, dto.membersPayoff);
          
    return createdExpense.save();
  }

  async update(expense: UpdateExpenseDto): Promise<ExpenseDto> {
    return this.expenseModel.findOneAndUpdate({ _id: expense.id }, {name: expense.name}, { new: false, upsert:true, setDefaultsOnInsert: true } ).exec();
  }

  async findAll(): Promise<ExpenseDto[]> {
    return this.expenseModel.find().populate('members').populate('group').exec();
  }

  async findAllByGroup(id): Promise<ExpenseDto[]> {
    return this.expenseModel.find({group: id}).exec();
  }

  async find(id): Promise<ExpenseDto> {
    return this.expenseModel.findById({_id: id }).populate('members').exec();
  }

  async delete(id: string): Promise<ExpenseDto>{
    return this.expenseModel.findOneAndRemove({_id: id}).exec();
  }

}
