import { expensesProviders } from './providers/expenses.providers';
import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { ExpensesController } from './expenses.controller';
import { ExpensesService } from './expenses.service';
import { GroupsModule } from '../groups/groups.module';

@Module({
  imports: [DatabaseModule, GroupsModule],
  controllers: [ExpensesController],
  providers: [
    ExpensesService,
    ...expensesProviders,
  ]
})
export class ExpensesModule {}
