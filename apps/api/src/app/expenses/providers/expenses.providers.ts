
import { Connection } from 'mongoose';
import { ExpenseSchema } from '../../database/schemas/expense.schema';

export const expensesProviders = [
  {
    provide: 'EXPENSE_MODEL',
    useFactory: (connection: Connection) => connection.model('Expense', ExpenseSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
