import { Controller, Get, Post, Body, Delete, Patch, Query, Param, ParseIntPipe } from '@nestjs/common';
import { ExpensesService } from './expenses.service';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { CreateExpenseDto, ExpenseDto } from '@split-it/api-interfaces';

@Controller('expenses')
export class ExpensesController {
  constructor(private readonly expensesService: ExpensesService) {}

  @Post()
  @ApiResponse({ status: 201, description: 'The expense has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(@Body() createExpenseDto: CreateExpenseDto) {
    await this.expensesService.create(createExpenseDto);
  }

  @Patch()
  async update(@Body() updateExpenseDto: UpdateExpenseDto): Promise<ExpenseDto> {
    return this.expensesService.update(updateExpenseDto);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<ExpenseDto> {
    return this.expensesService.find(id);
  }

  @Get()
  async findAll(): Promise<ExpenseDto[]> {
    return this.expensesService.findAll();
  }

  @Get('group/:id')
  async findAllByGroup(@Param('id') id: string): Promise<ExpenseDto[]> {
    return this.expensesService.findAllByGroup(id);
  }


  @Delete()
  async delete(@Body() id: string): Promise<ExpenseDto> {
    return this.expensesService.delete(id);
  }
}
