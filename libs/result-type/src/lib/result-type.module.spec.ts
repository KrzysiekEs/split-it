import { async, TestBed } from '@angular/core/testing';
import { ResultTypeModule } from './result-type.module';

describe('ResultTypeModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ResultTypeModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ResultTypeModule).toBeDefined();
  });
});
