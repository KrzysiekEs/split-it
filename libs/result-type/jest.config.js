module.exports = {
  name: 'result-type',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/result-type',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
