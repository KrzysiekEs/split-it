
export interface ExpenseDto {
  description: string;
  type: string;
  date: Date;
  buyerIndex: number;
  membersPayoff: number[],
  groupId: string
}
