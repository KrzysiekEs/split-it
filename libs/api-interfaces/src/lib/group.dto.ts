import { AddGroupDTO } from './add-group.dto';
import { StateDTO } from './state.dto';

export interface GroupDTO extends AddGroupDTO {
  _id?: string;
  state?: StateDTO
}
