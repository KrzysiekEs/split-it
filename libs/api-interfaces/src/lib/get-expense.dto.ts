import { Currency } from './currencies.enum';

export interface GetExpenseDto {
  description: string;
  type: string;
  date: Date;
  buyerIndex: number;
  membersPayoff: number[],
  groupId: string
}
