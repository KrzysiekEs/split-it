import { GroupType } from './types/group-type';

export interface AddGroupDTO {
    name: string,
    members: string[]
    owner: string;
    type: GroupType;
}