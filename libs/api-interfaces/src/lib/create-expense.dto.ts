export interface CreateExpenseDto {
  description: string;
  type: string;
  date: Date;
  buyerIndex: number;
  membersPayoff: number[],
  groupId: string
}
